package id.co.hris.hrisgo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.zip.Inflater;

import id.co.hris.hrisgo.Model.Attendance;

/**
 * Created by rio senjou on 22/04/2018.
 */

public class AdapterAttendance extends BaseAdapter {
    private Context context;
    private ArrayList<Attendance> atdItem = new ArrayList<>();
    private LayoutInflater mInflater;

    public AdapterAttendance(Context context,ArrayList<Attendance> atdItem) {
        this.atdItem = atdItem;
        this.context = context;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return atdItem.size();
    }

    @Override
    public Object getItem(int position) {
        return atdItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        MyViewHolder myHolder = null;
        if (convertView == null){
            convertView = mInflater.inflate(R.layout.item_attendance,null);
            myHolder = new MyViewHolder();
            myHolder.empNumber = (TextView)convertView.findViewById(R.id.tv_emp_number);
            myHolder.workDate = (TextView)convertView.findViewById(R.id.tv_work_date);
            myHolder.timeIn = (TextView)convertView.findViewById(R.id.tv_time_in);
            myHolder.timeOut = (TextView)convertView.findViewById(R.id.tv_time_out);
            convertView.setTag(myHolder);
        }else{
            myHolder = (MyViewHolder)convertView.getTag();
        }

        myHolder.empNumber.setText(atdItem.get(position).getEmp_number().toString());
        myHolder.workDate.setText(atdItem.get(position).getWork_date().toString());
        myHolder.timeIn.setText(atdItem.get(position).getTime_in().toString());
        myHolder.timeOut.setText(atdItem.get(position).getTime_out().toString());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Emp Number : "+atdItem.get(position).getEmp_number().toString()
                        + "\nWork Date : "+ atdItem.get(position).getWork_date().toString() +
                        "\nTime In : "+atdItem.get(position).getTime_in().toString() +
                        "\nTime Out : "+atdItem.get(position).getTime_out().toString(),Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }

    public class MyViewHolder{
        TextView empNumber,workDate,timeIn,timeOut;
    }
}
