package id.co.hris.hrisgo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.hris.hrisgo.Model.Attendance;
import id.co.hris.hrisgo.Model.Login;
import id.co.hris.hrisgo.Model.User;
import id.co.hris.hrisgo.Model.result;
import id.co.hris.hrisgo.Rest.ApiClient;
import id.co.hris.hrisgo.Rest.RestApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HalamanLogin extends AppCompatActivity {

    private EditText inputUsername, inputPassword;
    private CheckBox checkBox;

    SharedPreferences sPref;
    public final String keyNik ="KeyNik",
            keyEmail="KeyEmail";

    ArrayList<Attendance> results=new ArrayList<>();
    RestApi restApi;

    @OnClick(R.id.btnLogin)
    void btnLogin() {
        final String user, pass;
        final Intent i = new Intent(HalamanLogin.this, HomeUtama.class);
        Log.d("tes","Tes butterknife");
        user = inputUsername.getText().toString().trim();
        pass = inputPassword.getText().toString().trim();

        i.putExtra("noEmployee", inputUsername.getText().toString().trim());

        String md5 = MD5.md5(pass.toString());


//            rest api
        restApi = ApiClient.getClient().create(RestApi.class);
        Call<Login> call = restApi.login(user, md5, "login");

        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Boolean error = response.body().isError();
//                results  =response.body().getData();

                if (false == error) {
                    Log.d("nik", response.body().getNik());

                    User usr = response.body().getUsr();
                    String empName = usr.getEmp_number();
                    String email = usr.getEmail();

                    SharedPreferences.Editor editor = sPref.edit();
                    editor.putString(keyNik,response.body().getNik());
                    editor.putString(keyEmail,email);
                    editor.apply();

                    Toast.makeText(HalamanLogin.this, "NIK :" + empName + "\nemail :" + email, Toast.LENGTH_LONG).show();
//                        Masuk ke Halama Login
                    startActivity(i);
                } else {
                    Toast.makeText(HalamanLogin.this, "Username atau Password salah", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Toast.makeText(HalamanLogin.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_login);
        ButterKnife.bind(this);

//        Untuk penanda bahwa hrus ada header di sharedPreference
        sPref = getSharedPreferences("KEYPREF", Context.MODE_PRIVATE);

        inputUsername = (EditText) findViewById(R.id.username);
        inputPassword = (EditText) findViewById(R.id.password);
        checkBox = (CheckBox) findViewById(R.id.cbPassword);
        /*login = (Button) findViewById(R.id.btnLogin);
        cancel = (Button) findViewById(R.id.btnCancel);
*/


        if (checkBox.isChecked()) {
            inputPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        } else {
            inputPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        /*Membuat Aksi Ketika Checkbox Di Klik*/


    }



}
