package id.co.hris.hrisgo.Model;

import com.google.gson.annotations.SerializedName;

public class Attendance {

    @SerializedName("emp_number")
    private String emp_number;
    @SerializedName("work_date")
    private String work_date;
    @SerializedName("time_in")
    private String time_in;
    @SerializedName("time_out")
    private String time_out;

    public String getEmp_number(){return emp_number;};
    public void setEmp_number(String emp_number){this.emp_number = emp_number;}

    public String getWork_date(){return work_date;};
    public void setWork_date(String work_date){this.work_date = work_date;}

    public String getTime_in(){return time_in;};
    public void setTime_in(String time_in){this.time_in = time_in;}

    public String getTime_out(){return time_out;};
    public void setTime_out(String time_out){this.time_out = time_out;}
}
