package id.co.hris.hrisgo.Rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    public static final String URL = "http://hrisgo.baf.id:9191/";
    private static Retrofit retrofit = null;

    //    Untuk mengecek Struktru gson apa bila ada yang salah di perbaiki
    private static Gson gson = new GsonBuilder().setLenient().create();

    public static Retrofit getClient(){
        if (retrofit ==null){
            retrofit = new Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        }
        return retrofit;
    }
}
