package id.co.hris.hrisgo.Rest;

import id.co.hris.hrisgo.Model.Attendance;
import id.co.hris.hrisgo.Model.Login;
import id.co.hris.hrisgo.Model.result;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RestApi {

    @FormUrlEncoded
    @POST("isevisi/abasebenzisi/")
    Call<Login> login(@Field("umsebenzisi") String umsebenzisi,
                      @Field("iphasiwedi") String iphasiwedi,
                      @Field("imodi") String imodi);


    @FormUrlEncoded
    @POST("isevisi/hcm/")
    Call<result> attendance(@Field("inombolo_yomsebenzi") String inombolo_yomsebenzi,
                            @Field("ekuqaleni") String ekuqaleni,
                            @Field("ukuphela") String ukuphela,
                            @Field("isenzo") String isenzo,
                            @Field("ukuthengiselana") String ukuthengiselana);

}
