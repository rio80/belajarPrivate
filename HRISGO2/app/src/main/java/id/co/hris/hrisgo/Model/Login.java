package id.co.hris.hrisgo.Model;

public class Login   {

    private boolean error;
    private String nik;
    private User user;

    public User getUsr() {
        return user;
    }

    public void setUsr(User usr) {
        this.user = user;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }
}
