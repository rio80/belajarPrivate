package id.co.hris.hrisgo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ShowingAttendance extends AppCompatActivity implements View.OnClickListener {

    TextView getStartDateAttendance, getEndDateAttendance, getnik;
    private EditText stardatee, endatee, nikk;
    private Button btnAttd;
    private Intent i;

    private String nik;

    SharedPreferences sPref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showing_attendance);
        sPref = getSharedPreferences("KEYPREF", Context.MODE_PRIVATE);

        stardatee = (EditText)findViewById(R.id.sstardate);
        endatee = (EditText)findViewById(R.id.eendate);
        nikk = (EditText)findViewById(R.id.nnik);
        btnAttd = (Button)findViewById(R.id.btnAttd);
        HalamanLogin halLogin=new HalamanLogin();
        nikk.setText(sPref.getString(halLogin.keyNik,""));
        btnAttd.setOnClickListener(this);
        i = getIntent();

        getStartDateAttendance = (TextView)findViewById(R.id.get_startDateAtd);
        getEndDateAttendance = (TextView)findViewById(R.id.get_endDateAtd);
//        getnik = (TextView)findViewById(R.id.get_noEmployee);

        getStartDateAttendance.setText(i.getExtras().getString("StartDateAttendance","tidak ada inputan"));
        getEndDateAttendance.setText(i.getExtras().getString("EndDateAttendance","tidak ada inputan"));

        stardatee.setText(getStartDateAttendance.getText().toString());
        endatee.setText(getEndDateAttendance.getText().toString());

//        getnik.setText(i.getExtras().getString("noEmployee","no Inputan Brohh"));


    }

    @Override
    public void onClick(View v) {
        if (v.getId()== R.id.btnAttd){
            Intent i =new Intent(ShowingAttendance.this,ListAttendance.class);
//            final Intent i = new Intent(ShowingAttendance)
            String nik,startDate,endDate;
            nik = nikk.getText().toString();
            startDate = stardatee.getText().toString();
            endDate = endatee.getText().toString();
            i.putExtra("nik",nik);
            i.putExtra("start_date",startDate);
            i.putExtra("end_date",endDate);

            startActivity(i);
        }
    }
}
