package id.co.hris.hrisgo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import id.co.hris.hrisgo.Model.Attendance;
import id.co.hris.hrisgo.Model.result;
import id.co.hris.hrisgo.Rest.ApiClient;
import id.co.hris.hrisgo.Rest.RestApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListAttendance extends AppCompatActivity {
    ListView lvAttendance;
    RestApi restApi;
    ArrayList<Attendance> attendances;
    AdapterAttendance adapter;
    Intent i;
    String nik,startDate,endDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_attendance);
        lvAttendance = (ListView)findViewById(R.id.lv_attendance);
        attendances = new ArrayList<>();


        i = getIntent();
        nik = i.getStringExtra("nik");
        startDate = i.getStringExtra("start_date");
        endDate = i.getStringExtra("end_date");

        Toast.makeText(getApplicationContext(),"Ini list attendace\nnik : "+nik
                + "\nStart date : "+ startDate +
                "\nEnd date : "+endDate,Toast.LENGTH_SHORT).show();

        restApi = ApiClient.getClient().create(RestApi.class);
        Call<result> call = restApi.attendance(nik,startDate,endDate,"get","ukufika_det");
        call.enqueue(new Callback<result>() {
            @Override
            public void onResponse(Call<result> call, Response<result> response) {
                boolean result = response.body().isError();
                Log.d("result",String.valueOf(result));
                if (false==result){
                    attendances  =response.body().getData();
                    Toast.makeText(getApplicationContext(),"Emp Number : "+attendances.get(0).getEmp_number().toString()
                            + "\nWork Date : "+ attendances.get(0).getWork_date().toString() +
                            "\nTime In : "+attendances.get(0).getTime_in().toString() +
                            "\nTime Out : "+attendances.get(0).getTime_out().toString(),Toast.LENGTH_SHORT).show();

                    adapter = new AdapterAttendance(ListAttendance.this,attendances);
                    lvAttendance.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<result> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"error di : "+t.getMessage().toString(),Toast.LENGTH_SHORT).show();
            }
        });


    }
}
