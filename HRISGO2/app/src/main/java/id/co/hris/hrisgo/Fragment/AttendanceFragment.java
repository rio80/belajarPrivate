package id.co.hris.hrisgo.Fragment;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import id.co.hris.hrisgo.HalamanLogin;
import id.co.hris.hrisgo.R;
import id.co.hris.hrisgo.ShowingAttendance;

/**
 * A simple {@link Fragment} subclass.
 */
public class AttendanceFragment extends Fragment implements View.OnClickListener {


    private Button btnStartDate, btnEndDate, btnEnter;
    private TextView tvStartDate, tvEndDate;
    private DatePickerDialog datePickerDialog;
    private Calendar calendar;
    private int hari, bulan, tahun;
    private String nik;

    SharedPreferences sPref;
    HalamanLogin login=new HalamanLogin();

    public AttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_attendance, container, false);
        btnStartDate = (Button) v.findViewById(R.id.startDate);
        btnEndDate = (Button) v.findViewById(R.id.endDate);
        btnEnter = (Button) v.findViewById(R.id.btn_enter);
        tvStartDate = (TextView) v.findViewById(R.id.tvStartdate);
        tvEndDate = (TextView) v.findViewById(R.id.tvEnddate);




        btnStartDate.setOnClickListener(this);
        btnEndDate.setOnClickListener(this);
        btnEnter.setOnClickListener(this);
        return v;
    }


    @Override
    public void onClick(View v) {

        //Parameter untuk Stardate Attendance
        if (v.getId() == R.id.startDate) {
            calendar = Calendar.getInstance();
            hari = calendar.get(Calendar.DAY_OF_MONTH);
            bulan = calendar.get(Calendar.MONTH);
            tahun = calendar.get(Calendar.YEAR);

            datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    String tgl;
                    String bulan = String.valueOf(month + 1);
                    if (bulan.length() < 2){
                        bulan = "0"+bulan;
                    }
                    String hari = String.valueOf(dayOfMonth);
                    if (hari.length() < 2){
                        hari = "0"+hari;
                    }
                    tgl = String.valueOf(year + "/" + bulan + "/" + hari);

                    tvStartDate.setText(tgl.toString());
                }
            }, tahun, bulan, hari);
            datePickerDialog.show();
        }
        //Parameter untuk Stardate Attendance


        //Parameter untuk Enddate Attendance
        if (v.getId() == R.id.endDate) {
            calendar = Calendar.getInstance();
            hari = calendar.get(Calendar.DAY_OF_MONTH);
            bulan = calendar.get(Calendar.MONTH);
            tahun = calendar.get(Calendar.YEAR);

            datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    String tgl;
                    String bulan = String.valueOf(month + 1);
                    if (bulan.length() < 2){
                        bulan = "0"+bulan;
                    }
                    String hari = String.valueOf(dayOfMonth);
                    if (hari.length() < 2){
                        hari = "0"+hari;
                    }
                    tgl = String.valueOf(year + "/" + bulan + "/" + hari);

                    tvEndDate.setText(tgl.toString());
                }
            }, tahun, bulan, hari);
            datePickerDialog.show();
        }
        //Parameter untuk Enddate Attendance


        //Button untuk menampilkan Attendance sesuai dengan Parameter Stardate Endate
        if (v.getId() == R.id.btn_enter) {
            Intent i = new Intent(getActivity(), ShowingAttendance.class);
            i.putExtra("StartDateAttendance", tvStartDate.getText().toString());
            i.putExtra("EndDateAttendance", tvEndDate.getText().toString());
            getActivity().startActivity(i);
        }
        //Button untuk menampilkan Attendance sesuai dengan Parameter Stardate Endate


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sPref = getContext().getSharedPreferences("KEYPREF", Context.MODE_PRIVATE);
        nik =sPref.getString(login.keyNik,"");
        Toast.makeText(getContext(), nik.toString(), Toast.LENGTH_SHORT).show();
    }
}
