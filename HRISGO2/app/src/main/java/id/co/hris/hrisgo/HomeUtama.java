package id.co.hris.hrisgo;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import id.co.hris.hrisgo.Fragment.AttendanceFragment;
import id.co.hris.hrisgo.Fragment.ComingSoonFragment;
import id.co.hris.hrisgo.Fragment.HomeFragment;
import id.co.hris.hrisgo.Fragment.ProfileFragment;

public class HomeUtama extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;
    private FrameLayout frameLayout;

    private HomeFragment homeFragment;
    private ProfileFragment profileFragment;
    private AttendanceFragment attendanceFragment;
    private ComingSoonFragment comingSoonFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_utama);

        frameLayout = (FrameLayout) findViewById(R.id.main_frame);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.main_nav);

//      FRAGMENT
        homeFragment = new HomeFragment();
        profileFragment = new ProfileFragment();
        attendanceFragment = new AttendanceFragment();
        comingSoonFragment = new ComingSoonFragment();

//        default ketika masuk kehalaman HomeUtama
        setFragment(homeFragment);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.nav_home:
//                        bottomNavigationView.setItemBackgroundResource(R.color.colorPrimaryDark);
                        setFragment(homeFragment);
                        return true;

                    case R.id.nav_profile:
//                        bottomNavigationView.setItemBackgroundResource(R.color.colorPrimaryDark);
                        setFragment(profileFragment);
                        return true;

                    case R.id.nav_attendance:
//                        bottomNavigationView.setItemBackgroundResource(R.color.colorPrimaryDark);
                        setFragment(attendanceFragment);
                        return true;

                    case R.id.nav_coming_soon:
//                        bottomNavigationView.setItemBackgroundResource(R.color.colorPrimaryDark);
                        setFragment(comingSoonFragment);
                        return true;

                    default:
                        return false;

                }

            }


        });
    }

    private void setFragment(android.support.v4.app.Fragment fragment) {

       android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
       fragmentTransaction.replace(R.id.main_frame,fragment);
       fragmentTransaction.commit();
    }
}
