package id.co.hris.hrisgo.Model;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by rio senjou on 14/04/2018.
 */

public class result {
    public ArrayList<Attendance> data;
    public boolean error;

    public result(ArrayList<Attendance> data, boolean error) {
        this.data = data;
        this.error = error;
    }

    public ArrayList<Attendance> getData() {
        return data;
    }

    public void setData(ArrayList<Attendance> data) {
        this.data = data;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
